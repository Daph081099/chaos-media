import {Serveur} from "../public/scripts/Serveur.js";
import {GLTFLoader} from "../bibliotheques/GLTFLoader.js";

export let scene, camera, renderer;
let video;
let physics_stats, render_stats;
let ground;
let ecouteurTick;
let loader;
export let model, panier, ballonPris, ballonLancer, clientConnect, posBallonInitX, posBallonInitY, posBallonInitZ;
export let event;


let vector;

let mouse = {x: 0, y: 0};

export class Application {

    constructor() {

        Physijs.scripts.worker = '../bibliotheques/physijs_worker.js';
        Physijs.scripts.ammo = 'ammo.js';

        nw.Window.get().showDevTools();

        this.serveur = new Serveur();

        this.serveur.demarrer();

        this.initLoader();
        this.initThreeJS();

        this.ecouteur = this.bougerBallon.bind(this);


    }

    initLoader() {

        loader = new GLTFLoader();

        loader.load('../ressources/panier-neon.glb', function (gltf) {

            panier = gltf.scene.children[0];
            scene.add(gltf.scene);

        }, undefined, function (error) {
            // console.error(error);

        });

    }

    initThreeJS() {
        scene = new Physijs.Scene({fixedTimeStep: 1 / 120});
        scene.setGravity(new THREE.Vector3(0, -30, 0));
        scene.addEventListener(
            'update',
            function () {
                scene.simulate(undefined, 2);
                physics_stats.update();
            }
        );

        camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);


        renderer = new THREE.WebGLRenderer({antialias: true});
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderer.shadowMap.enabled = true;
        renderer.shadowMapSoft = true;
        document.body.appendChild(renderer.domElement);

        render_stats = new Stats();
        render_stats.domElement.style.position = 'absolute';
        render_stats.domElement.style.top = '0px';
        render_stats.domElement.style.zIndex = 100;
        document.body.appendChild(render_stats.domElement);

        physics_stats = new Stats();
        physics_stats.domElement.style.position = 'absolute';
        physics_stats.domElement.style.top = '50px';
        physics_stats.domElement.style.zIndex = 100;
        document.body.appendChild(physics_stats.domElement);


        this.ajouterVideo();
        this.ajouterElements();
    }

    ajouterElements() {
        //Lumière ambiante

        let ambient = new THREE.AmbientLight(0x000000, 0.3);
        scene.add(ambient)
        let pointLight = new THREE.PointLight(0xffffff, 0.6, 200);
        pointLight.position.set(10, 10, 100);
        pointLight.castShadow = true;
        scene.add(pointLight);

        //Lumière directionnelle

        let dirLight = new THREE.DirectionalLight(0xffffff, 1, 100);
        dirLight.position.set(20, 15, 2);
        dirLight.castShadow = true;
        scene.add(dirLight);

        //Plancher
        // new THREE.MeshBasicMaterial({color: 0xbababa, opacity: 0, transparent: true});
        let material = new Physijs.createMaterial( new THREE.MeshBasicMaterial({color: 0x888888, opacity: 0, transparent: true}), 0.8, 1);
        ground = new Physijs.BoxMesh(
            new THREE.BoxGeometry(30, 0.3, 100),
            material,
            0 // mass
        );

        ground.position.y = -10;
        ground.position.z = -30;
        ground.receiveShadow = true;
        ground.rotation.x = 0.04;
        scene.add(ground);

        //BOUNDS COLLISION

        let colPanneau = new Physijs.BoxMesh(
            new THREE.BoxGeometry(13, 8, 0.3),
            material,
            0 // mass
        );
        colPanneau.position.z = -26;
        colPanneau.position.y = 10;
        scene.add(colPanneau);

        let colRim01 = new Physijs.BoxMesh(
            new THREE.BoxGeometry(0.2, 0.2, 8),
            material,
            0 // mass
        );

        colRim01.position.z = -26;
        colRim01.position.y = 7;
        colRim01.position.x = -3;
        scene.add(colRim01);

        let colRim02 = new Physijs.BoxMesh(
            new THREE.BoxGeometry(0.2, 0.2, 8),
            material,
            0 // mass
        );

        colRim02.position.z = -26;
        colRim02.position.y = 7;
        colRim02.position.x = 3;
        scene.add(colRim02);

        let colRim03 = new Physijs.BoxMesh(
            new THREE.BoxGeometry(6, 0.2, 0.2),
            material,
            0 // mass
        );

        colRim03.position.z = -21;
        colRim03.position.y = 7;

        scene.add(colRim03);

        //Ballon
        let sphere_geometry = new THREE.SphereGeometry(2, 32, 32);

        let materialModel = new Physijs.createMaterial( new THREE.MeshBasicMaterial({color: 0xFE5555}), 0, 1);
        model = new Physijs.SphereMesh(sphere_geometry, materialModel, 10);
        model.position.set(0, 0, 0);
        model.setCcdMotionThreshold(1);
        model.setCcdSweptSphereRadius(0.2);

        posBallonInitX = model.position.x;
        posBallonInitY = model.position.y;
        posBallonInitZ = model.position.z;

        scene.add(model);

        model.addEventListener('lancer', this.lancerBallon.bind(this));

        camera.position.z = 10;

        this.animate();

        // document.addEventListener('mouseup', this.lancerBallon.bind(this));
        document.addEventListener('mousedown', this.prendreBallon.bind(this));

    }

    ajouterVideo() {

        let video, videoImage, videoImageContext, videoTexture;

        init();
        animate();

        // FUNCTIONS
        function init() {
            video = document.createElement('video');
            video.type = 'video';
            video.src = "ressources/arcade_video.webm";
            video.load(); // must call after setting/changing source
            video.play();
            video.loop = true;

            videoImage = document.createElement('canvas');
            videoImage.width = 1280;
            videoImage.height = 720;

            videoImageContext = videoImage.getContext('2d');
            // background color if no video present
            videoImageContext.fillRect(0, 0, videoImage.width, videoImage.height);

            videoTexture = new THREE.Texture(videoImage);
            videoTexture.minFilter = THREE.LinearFilter;
            videoTexture.magFilter = THREE.LinearFilter;

            let movieMaterial = new THREE.MeshBasicMaterial({
                map: videoTexture,
                overdraw: true,
                side: THREE.DoubleSide
            });
            let movieGeometry = new THREE.PlaneGeometry(128, 72, 4, 4);
            let movieScreen = new THREE.Mesh(movieGeometry, movieMaterial);
            movieScreen.position.set(0, 0, -50);
            scene.add(movieScreen);

        }

        function animate() {
            requestAnimationFrame(animate);
            render();
            // update();
        }

        function render() {
            if (video.readyState === video.HAVE_ENOUGH_DATA) {
                videoImageContext.drawImage(video, 0, 0, 1280, 720);
                if (videoTexture)
                    videoTexture.needsUpdate = true;
            }

            renderer.render(scene, camera);
        }
    }

    bougerBallon(event) {

        if (!ballonLancer) {

            event.preventDefault();
            mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
            mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
            vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
            vector.unproject(camera);
            let dir = vector.sub(camera.position).normalize();
            let distance = -camera.position.z / dir.z;
            let pos = camera.position.clone().add(dir.multiplyScalar(distance));
            model.position.copy(pos);
            model.__dirtyPosition = true;

        }
    }

    prendreBallon() {
        document.addEventListener('mousemove', this.ecouteur);
    }

    lancerBallon(temps, posXinit, posYinit, posXfinal, posYfinal ) {

        if (!ballonLancer) {
            document.removeEventListener('mousemove', this.ecouteur);
            // console.log("Ballon Relacher et lancer");

            ballonPris = true;
            ballonLancer = true;

            // Math.pow(temps, 2)
            let tempsParcours = temps/100;

            let distanceY = (posYinit - posYfinal)/2;

            let velocite = distanceY / tempsParcours;
            console.log(velocite);


            model.setLinearVelocity(new THREE.Vector3(0, velocite, -velocite));
            model.setAngularVelocity(new THREE.Vector3(0, 1, 0));

            setTimeout(() => {

                model.__dirtyPosition = false;
                model.position.set(posBallonInitX, posBallonInitY, posBallonInitZ);
                ballonPris = false;
                ballonLancer = false;
                model.__dirtyPosition = true;

            }, 7000);
        }
    }

    animate() {
        if (model !== undefined) {

            if (ballonPris !== true) {

                model.__dirtyPosition = true;
                model.setLinearVelocity(new THREE.Vector3(0, 0, 0));
                model.setAngularVelocity(new THREE.Vector3(0, 0, 0));

            }
        }

        if (panier !== undefined) {

            panier.scale.set(3, 3, 3);
            panier.position.z = -35;
            panier.position.y = -10;
            panier.rotation.y = -Math.PI / 2;

        }

        // if(panier !== undefined){
        //     this.positionnerPanier();
        // }

        scene.simulate();
        renderer.render(scene, camera);
        requestAnimationFrame(this.animate.bind(this));
        render_stats.update();
    }

}