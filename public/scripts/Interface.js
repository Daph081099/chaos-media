export let ballon, canvas;
export let tempsToucher = 0, ballonPosXinit, ballonPosYinit, ballonPosXfinal, ballonPosYfinal, peutLancer = true;

export class Interface {

    constructor() {

        this.socket = io("http://192.168.0.138:3000");

        document.body.addEventListener("touchmove", this.gererMouvement.bind(this));
        document.body.addEventListener("touchend", this.gererFinMouvement.bind(this));
        document.body.addEventListener("touchstart", this.gererDebutMouvement.bind(this));

        this.stage = null;

        canvas = document.querySelector('canvas');

        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;

        canvas.style.backgroundColor = '#01002C';

        this.parametres = {
            manifeste: "ressources/manifest.json"
        };

        this.charger();

        this.timer = null;

    }

    charger() {

        this.chargeur = new createjs.LoadQueue();
        this.chargeur.installPlugin(createjs.Sound);
        this.chargeur.addEventListener("complete", this.creerStage.bind(this));
        this.chargeur.addEventListener('error', this.abandonner.bind(this));
        this.chargeur.loadManifest(this.parametres.manifeste);

    }

    abandonner(e) {
        alert("L'élément suivant n'a pu être chargé: " + e.src);
    }

    gererMouvement(e) {

        if (peutLancer) {

            if(e.touches[0].clientY < canvas.height/2){
                if(e.touches[0].clientX < canvas.width/2){
                    this.socket.emit("position", {x:-((canvas.width/2) - e.touches[0].clientX) *5, y: -((canvas.height/2) - e.touches[0].clientY) *2});
                }
                else if(e.touches[0].clientX > canvas.width/2){
                    this.socket.emit("position", {x: (e.touches[0].clientX -canvas.width/2) *5, y: -((canvas.height/2) - e.touches[0].clientY) *2});
                }
            }
            else if(e.touches[0].clientY > canvas.height/2){
                if(e.touches[0].clientX < canvas.width/2){
                    this.socket.emit("position", {x:-((canvas.width/2) - e.touches[0].clientX) *5, y: (e.touches[0].clientY -canvas.height/2) *2});
                }
                else if(e.touches[0].clientX > canvas.width/2){
                    this.socket.emit("position", {x: (e.touches[0].clientX -canvas.width/2) *5, y: (e.touches[0].clientY -canvas.height/2) *2});
                }
            }

            // this.socket.emit("position", {x: e.touches[0].clientX, y: e.touches[0].clientY});

            ballon.x = e.touches[0].clientX;
            ballon.y = e.touches[0].clientY;
        }
    }

    gererDebutMouvement() {

        if (peutLancer) {

            ballonPosXinit = ballon.x;
            ballonPosYinit = ballon.y;

            this.timer = setInterval(() => {
                tempsToucher += 1
            }, 1);

        }
    }

    gererFinMouvement() {

        if (peutLancer) {
            peutLancer = false;

            // console.log('fin du mouvement');

            clearInterval(this.timer);

            ballonPosXfinal = ballon.x;
            ballonPosYfinal = ballon.y;

            this.lancerBallonInterfaceMobile(tempsToucher, ballonPosXinit, ballonPosYinit, ballonPosXfinal, ballonPosYfinal);
            this.socket.emit('lancer', {temps: tempsToucher, posXinit: ballonPosXinit, posYinit: ballonPosYinit,posXfinal: ballonPosXfinal, posYfinal: ballonPosYfinal});


            // ballonPosYinit = ballonPosXinit = ballonPosYfinal = ballonPosXfinal = canvas.height - 100;

            tempsToucher = 0;

            setTimeout(() => {
                ballon.x = canvas.width / 2
                ballon.y = canvas.height - 120;
                // console.log('timer fonctionne?')
                peutLancer = true;

            }, 3000)
        }
    }


    creerStage() {

        this.stage = new createjs.StageGL(canvas, {'antialias': true});
        this.stage.setClearColor('#01002C');

        createjs.Ticker.addEventListener('tick', e => this.stage.update(e));
        createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
        createjs.Ticker.framerate = 60;

        this.ajouterPlateforme();
        this.ajouterBallon();

    }

    ajouterBallon() {
        ballon = new createjs.Bitmap(this.chargeur.getResult('ballon'));
        this.stage.addChild(ballon);

        ballon.regX = ballon.getBounds().width / 2;
        ballon.regY = ballon.getBounds().height / 2;

        ballon.x = canvas.width / 2;
        ballon.y = canvas.height - 120;

    }

    ajouterPlateforme() {
        let plateforme = new createjs.Bitmap(this.chargeur.getResult('plateforme'));
        this.stage.addChild(plateforme);

        plateforme.regX = plateforme.getBounds().width / 2;
        plateforme.regY = plateforme.getBounds().height / 2;

        plateforme.width = window.innerWidth;

        plateforme.x = canvas.width / 2;
        plateforme.y = canvas.height - 120;
        plateforme.scale = 0.85;

    }

    lancerBallonInterfaceMobile(tempsToucherVelo, pXinit, pYinit, pXfinal, pYfinal) {

        let distanceY = pYinit - pYfinal;

        // console.log("position début = " + pYinit);
        // console.log("position Fin   = " + pYfinal);

        if (distanceY < 0) {

        } else {
            let velocite = distanceY / tempsToucherVelo;
            // console.log('velocite = ' + velocite);

            createjs.Tween.get(ballon)
                .to({y: -200}, 2000)

        }


    }
}