import {model} from "../../scripts/Application.js";
import {event} from "../../scripts/Application.js";

const express = require('express');
const http = require('http');

export class Serveur {

    constructor() {

        this.app = express();
        this.serveur = http.createServer(this.app);
        this.io = require('socket.io')(this.serveur);

        this.app.use(express.static('public'));

        this.event = event;

    }

    demarrer() {

        this.serveur.listen(3000, '192.168.0.138', () => {
            console.log('Le Serveur est prêt');
        });

        this.io.on("connection", socket => {

            console.log("Connexion d'un client");
            socket.on("position", this.deplacer.bind(this));
            socket.on("lancer", this.lancerBallon.bind(this));

        });

    }

    deplacer(message) {

        model.position.y = message.y / -180;
        model.position.x = message.x / 180;

    }

    lancerBallon(message) {
        model.dispatchEvent("lancer", message.temps, message.posXinit, message.posYinit, message.posXfinal, message.posYfinal);
    }

}